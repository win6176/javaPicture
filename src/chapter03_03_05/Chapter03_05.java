package chapter03_03_05;

import java.util.Scanner;

public class Chapter03_05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in); 
		System.out.println("키와 몸무게를 입력하십시오.");
		float height = scanner.nextFloat();
		float weight = scanner.nextFloat();
		System.out.println("키는  " + height + "센티미터입니다.");
		System.out.println("몸무게는 " + weight + "킬로그램입니다.");
	}
}
